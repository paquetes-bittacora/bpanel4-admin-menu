@foreach ($adminMenu as $group)
        @if(!empty($group->modules))
        <li class="nav-item">
            <a href="#" class="nav-link dropdown-toggle collapsed">
                @if (!empty($group->icon))
                    <i class="fa-fw nav-icon {{$group->icon}}"></i>
                @endif
                <span class="nav-text fadeable">
                    <span>{{$group->title}}</span>
                </span>
                <b class="caret fa fa-angle-left rt-n90"></b>
            </a>
            @if (!empty($group->modules))
                <div class="hideable submenu collapse">
                    <ul class="submenu-inner">
                        @foreach ($group->modules as $module)
                            <li class="nav-item">
                                <a href="#" class="nav-link @if(!empty($module->actions)) dropdown-toggle @endif">
                                    @if (!empty($module->icon))
                                        <i class="fa-fw nav-icon {{$module->icon}}"></i>
                                    @endif
                                    <span class="nav-text">
                                        <span>
                                            {{ $module->title }}
                                        </span>
                                    </span>
                                    @if(!empty($module->actions))
                                        <b class="caret fa fa-angle-left rt-n90"></b>
                                    @endif
                                </a>
                                @if(!empty($module->actions))
                                    <div class="hideable submenu collapse">
                                        <ul class="submenu-inner">
                                        @foreach($module->actions as $action)
                                            <li class="nav-item @if(Route::is($action->route)) active @endif">
                                                <a href="{{ route($action->route) }}" class="nav-link">
                                                    @if (!empty($action->icon))
                                                        <i class="fa-fw nav-icon {{$action->icon}}"></i>
                                                    @endif
                                                    <span class="nav-text">
                                                        <span>
                                                            {{ $action->title }}
                                                        </span>
                                                    </span>
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                    </div>
                                @endif
                            </li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </li>
        @endif
@endforeach

@section('scripts')
    <script>
        $(document).ready(function(){
            $(".nav-item.active").parentsUntil("#sidebar").addClass(['active', 'open']);
            $(".nav-item.active").parents(".submenu").addClass("show");
        });
    </script>
@endsection
