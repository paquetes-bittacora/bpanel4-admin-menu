<?php

use Bittacora\AdminMenu\Database\Seeders\AdminMenuSeeder;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Schema;

class CreateAdminMenuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin-menu_groups', function (Blueprint $table) {
            $table->string('key')->primary();
            $table->string('title');
            $table->string('icon');

            $table->timestamps();
        });

        Schema::create('admin-menu_modules', function (Blueprint $table) {
            $table->string('key')->primary();
            $table->string('parent_key');
            $table->string('title');
            $table->string('icon')->nullable();
            $table->timestamps();

            $table->foreign('parent_key')->references('key')
                ->on('admin-menu_groups')->cascadeOnDelete();

        });

        Schema::create('admin-menu_actions', function (Blueprint $table) {
            $table->string('route')->primary();
            $table->string('parent_key');
            $table->string('title');
            $table->string('permission')->nullable();
            $table->string('icon')->nullable();
            $table->timestamps();

            $table->foreign('parent_key')->references('key')
                ->on('admin-menu_modules')->cascadeOnDelete();
        });

        // No llamamos a los seeders en los test porque los ralentizan
        // y no es necesario
        if (!App::runningUnitTests()) {
            Artisan::call('db:seed', [
                '--class' => AdminMenuSeeder::class,
                '--force' => true
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();

        Schema::drop('admin-menu_actions');
        Schema::drop('admin-menu_modules');
        Schema::drop('admin-menu_groups');

        Schema::enableForeignKeyConstraints();
    }
}
