<?php

namespace Bittacora\AdminMenu\Database\Seeders;

use Bittacora\AdminMenu\AdminMenuFacade;
use Illuminate\Database\Seeder;

class AdminMenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        AdminMenuFacade::createGroup('configuration', 'Configuración', 'fa fa-cog');
        AdminMenuFacade::createGroup('contents', 'Contenidos', 'fas fa-text');
    }
}
