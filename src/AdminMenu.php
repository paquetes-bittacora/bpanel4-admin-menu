<?php

namespace Bittacora\AdminMenu;

use App\Models\User;
use Bittacora\AdminMenu\Models\ActionModel;
use Bittacora\AdminMenu\Models\GroupModel;
use Bittacora\AdminMenu\Models\ModuleModel;
use Cache;
use Illuminate\Support\Facades\Session;

class AdminMenu
{
    /**
     * @param string $module Key del módulo al que pertenece la acción
     * @param string $title Título de la acción
     * @param string|null $permission Nombre del permiso
     * @param string|null $icon Icono de Font Awesome
     * @param string|null $route Ruta a la que llevará el enlace en el menú
     */
    public function createAction(
        string $module,
        string $title,
        ?string $permission = '',
        ?string $icon = '',
        ?string $route = null,
    ): ActionModel {
        $moduleModel = ModuleModel::findOrFail($module);

        $actionRoute = null === $route ? $module . '.' . $permission : $route;
        $action = $moduleModel->actions()->firstOrcreate([
            'route' => $actionRoute
        ], [
            'title' => $title,
            'icon' => $icon,
            'permission' => $permission
        ]);

        return $action;

    }

    /**
     * Añade un grupo al menú
     * @param string $key
     * @param string $title
     */
    public function createGroup(string $key, string $title, string $icon): GroupModel
    {
        return GroupModel::firstOrCreate([
            'key' => $key
        ], [
            'title' => $title,
            'icon' => $icon
        ]);

    }

    /**
     * Añade un módulo a un grupo del menú
     * @param string $group Key del grupo al que se va a añadir
     * @param string $module Key del módulo
     * @param string $title Título del módulo
     * @param string|null $icon Icono de font awesome
     */
    public function createModule(string $group, string $module, string $title, ?string $icon): ModuleModel
    {
        $groupModel = GroupModel::findOrFail($group);

        $groupModel->modules()->firstOrCreate([
            'key' => $module],[
            'title' => $title,
            'icon' => $icon
        ]);
        return ModuleModel::findOrFail($module);
    }

    public function getAdminMenu(User $user)
    {
        if (Session::has('adminMenu')) {
            $adminMenu = Session::get('adminMenu');
        } else {
            $userPermissions = $user->getAllPermissions()->pluck('name')->toArray();

            $adminMenu = GroupModel::get();

            foreach ($adminMenu as $key => $group) {
                foreach ($group->modules as $key2 => $module) {
                    $module->setRelations([
                        'actions' => $module->actions->reject(function ($value, $key) use ($userPermissions) {
                            return !in_array($value->route, $userPermissions);
                        })
                    ]);

                    if (!count($module->actions)) {
                        unset($group->modules[$key2]);
                    }
                }

                if (!count($group->modules) and isset($adminMenu[$key])) {
                    $adminMenu->forget($key);
                }
            }

            Session::put('adminMenu', $adminMenu);
        }

        return $adminMenu;
    }
}
