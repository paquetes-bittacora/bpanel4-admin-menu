<?php

namespace Bittacora\AdminMenu;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Bittacora\AdminMenu\AdminMenu
 */
class AdminMenuFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'admin-menu';
    }
}
