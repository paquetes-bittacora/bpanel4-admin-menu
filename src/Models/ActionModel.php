<?php

namespace Bittacora\AdminMenu\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ActionModel extends Model
{
    protected $primaryKey = 'route';
    public $incrementing = false;
    public $table = 'admin-menu_actions';
    protected $fillable = ['title', 'route', 'permission', 'icon'];

    public function module()
    {
        return $this->belongsTo(ModuleModel::class, 'parent_key');
    }
}
