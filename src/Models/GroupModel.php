<?php

declare(strict_types=1);

namespace Bittacora\AdminMenu\Models;

use Illuminate\Database\Eloquent\Model;

class GroupModel extends Model
{
    protected $primaryKey = 'key';
    public $incrementing = false;
    public $table = 'admin-menu_groups';
    protected $fillable = ['key', 'title', 'icon'];
    protected $with = ['modules'];

    public function modules()
    {
        return $this->hasMany(ModuleModel::class, 'parent_key');
    }
}
