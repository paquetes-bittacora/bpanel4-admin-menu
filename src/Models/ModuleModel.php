<?php

namespace Bittacora\AdminMenu\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ModuleModel extends Model
{
    protected $primaryKey = 'key';
    public $incrementing = false;
    public $table = 'admin-menu_modules';
    protected $fillable = ['key', 'title', 'icon'];
    protected $with = ['actions'];

    public function group()
    {
        return $this->belongsTo(GroupModel::class, 'parent_key');
    }

    public function actions(){
        return $this->hasMany(ActionModel::class, 'parent_key');
    }

}
