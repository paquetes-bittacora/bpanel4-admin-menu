<?php

namespace Bittacora\AdminMenu\View\Components;

use Bittacora\AdminMenu\AdminMenuFacade;
use Illuminate\View\Component;

class Sidebar extends Component
{
    private $sidebar;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->sidebar = AdminMenuFacade::getAdminMenu(auth()->user());
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('admin-menu::components.sidebar', ['sidebar' => $this->sidebar]);
    }
}
