<?php

namespace Bittacora\AdminMenu;

use Bittacora\AdminMenu\Http\Middleware\AdminMenuMiddleware;
use Bittacora\AdminMenu\View\Components\Sidebar;
use Illuminate\Routing\Router;
use Spatie\LaravelPackageTools\Package;
use Spatie\LaravelPackageTools\PackageServiceProvider;
use Bittacora\AdminMenu\Commands\AdminMenuCommand;


class AdminMenuServiceProvider extends PackageServiceProvider
{
    public function configurePackage(Package $package): void
    {
        /*
         * This class is a Package Service Provider
         *
         * More info: https://github.com/spatie/laravel-package-tools
         */
        $package
            ->name('admin-menu')
            ->hasConfigFile()
            ->hasViews()
            ->hasMigration('create_admin-menu_table')
            ->hasCommand(AdminMenuCommand::class);
    }

    public function register(){
        $this->app->bind('admin-menu', function($app){
            return new AdminMenu();
        });

    }

    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');
        $this->loadViewsFrom(__DIR__ .'/../resources/views', 'admin-menu');
        $this->loadViewComponentsAs('admin-menu', [
            Sidebar::class
        ]);

        $router = $this->app->make(Router::class);
        $router->aliasMiddleware('admin-menu', AdminMenuMiddleware::class);

    }

}
