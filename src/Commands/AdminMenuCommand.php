<?php

namespace Bittacora\AdminMenu\Commands;

use Illuminate\Console\Command;

class AdminMenuCommand extends Command
{
    public $signature = 'admin-menu';

    public $description = 'My command';

    public function handle()
    {
        $this->comment('All done');
    }
}
