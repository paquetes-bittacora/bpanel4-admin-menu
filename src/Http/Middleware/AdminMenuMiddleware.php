<?php

namespace Bittacora\AdminMenu\Http\Middleware;

use Bittacora\AdminMenu\AdminMenuFacade;
use Bittacora\Language\LanguageFacade;
use Bittacora\Language\Models\LanguageModel;
use Cache;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;

class AdminMenuMiddleware
{
    public function handle(Request $request, Closure $next){
        $userInSession = Auth::user();
        if(!Session::has('adminMenu')){
            $adminMenu = AdminMenuFacade::getAdminMenu($userInSession);
            View::share('adminMenu',$adminMenu);
        }else{
            View::share('adminMenu', Session::get('adminMenu'));
        }
        View::share('user', $userInSession);
        View::share('languages', LanguageFacade::getActives());

        return $next($request);
    }
}
