<!--- BEGIN HEADER -->
# Changelog

All notable changes to this project will be documented in this file.
<!--- END HEADER -->

## [0.0.1](https://gitlab.com/paquetes-bittacora/bpanel4-admin-menu/compare/00c667a5cef35a24e2d8d77e3db6a3457355cdbb...v0.0.1) (2021-11-08)
### Bug Fixes

* Corregir .gitignore ([ed3e13](https://gitlab.com/paquetes-bittacora/bpanel4-admin-menu/commit/ed3e133ab7d6c26d961af6198298409f9f8c4915))
* Corregir número de versión ([c90ecc](https://gitlab.com/paquetes-bittacora/bpanel4-admin-menu/commit/c90ecc315b602ac03767d86c4b2577d0f2ed8c83), [0c2594](https://gitlab.com/paquetes-bittacora/bpanel4-admin-menu/commit/0c2594a9e7520adc4a576fbf6dac58e579950b54))


---

## [](https://gitlab.com/paquetes-bittacora/bpanel4-admin-menu/compare/...v) (2021-11-08)

---

## [](https://gitlab.com/paquetes-bittacora/bpanel4-admin-menu/compare/...v) (2021-11-08)

---

## [](https://gitlab.com/paquetes-bittacora/bpanel4-admin-menu/compare/v...v) (2021-11-08)
### Bug Fixes

* Corregir .gitignore ([ed3e13](https://gitlab.com/paquetes-bittacora/bpanel4-admin-menu/commit/ed3e133ab7d6c26d961af6198298409f9f8c4915))
* Corregir número de versión ([0c2594](https://gitlab.com/paquetes-bittacora/bpanel4-admin-menu/commit/0c2594a9e7520adc4a576fbf6dac58e579950b54))


---

## [0.0.1](https://gitlab.com/paquetes-bittacora/bpanel4-admin-menu/compare/00c667a5cef35a24e2d8d77e3db6a3457355cdbb...v0.0.1) (2021-11-08)
### Bug Fixes

* Corregir .gitignore ([ed3e13](https://gitlab.com/paquetes-bittacora/bpanel4-admin-menu/commit/ed3e133ab7d6c26d961af6198298409f9f8c4915))


---

## [](https://gitlab.com/paquetes-bittacora/bpanel4-admin-menu/compare/v...v) (2021-11-08)
### Bug Fixes

* Corregir .gitignore ([ed3e13](https://gitlab.com/paquetes-bittacora/bpanel4-admin-menu/commit/ed3e133ab7d6c26d961af6198298409f9f8c4915))

---

## [](https://gitlab.com/paquetes-bittacora/bpanel4-admin-menu/compare/v...v) (2021-11-08)
### Bug Fixes

* Corregir .gitignore ([ed3e13](https://gitlab.com/paquetes-bittacora/bpanel4-admin-menu/commit/ed3e133ab7d6c26d961af6198298409f9f8c4915))


---

# Changelog

All notable changes to `admin-menu` will be documented in this file.

## 1.0.0 - 202X-XX-XX

- initial release
